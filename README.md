# Jubal Mabaquiao

Web Aficionado 

email: [jubal.mabaquiao@gmail.com](jubal.mabaquiao@gmail.com)
skype id: jubal.mabaquiao

Social Handles:

- [twitter.com/jubalmweb](https://twitter.com/jubalmweb)
- [github.com/jubalm](https://github.com/jubalm)
- [linkedin.com/in/jubalm](https://linkedin.com/in/jubalm)
- [jubal.mabaquiao.com](http://jubal.mabaquiao.com)

#### Personal Statement

A self-taught designer and developer with keen interest on the internet and its inner workings. With over 6 years of experience in web industries, from digital designs, to coding, to strategies and management, his passion for technology and design presents itself in his works and is certainly of great value.

#### Expertise

##### Web Development Skills

Jubal has deep knowledge in visual design trends. From typography, color harmony, usability and interfacing, mastering a wide array of tools for prototyping and design. He mostly enjoys conceptualizing in **Sketch**, **Photoshop**, **Illustrator**, and **FramerJS**.

A greater part of him can be summed up as turning visual design into web-compliant applications–primarily using **HTML**, **CSS** and **JavaScript**. His experience in web development has extended from front-end to full-stack by acquiring knowledge in **NodeJS**, **MongoDB**, **AngularJS**, and several other client-side libraries.

He has demonstrated skills as well in back-end programming handling a **Magento** e-commerce site which is built around **PHP** and **Zend**, and **MySQL**. At some point in his career, he also handled **server administration**; measuring website performance, metrics and optimization.

Lately, he has been caught playing around mobile app development, using hybrid platforms such as **Ionic**, **Cordova**, and **Fuse**.


##### Personal Skills

Possesses excellent communication and personal-interaction skills having good control of the English language. Has keen sense of detail and always keeps a positive mental attitude. This presents him the ability to translate technical ideas to non-technical audiences efficiently.

#### Work History 

**Isobar - Front-end Lead Developer**
_January 2015 – October 2015_

Determine strategies in building user experiences while keeping compliance. Keeping communication between developers, project managers and quality assurance personnel in aligning specifications. Spearheaded the research and development direction of the company, most notably authoring its coding standards as well as advocating workflow development, tooling, techniques and best practices.

**Isobar - Front-end Developer**
_January 2014 – January 2015_

Tapped the full potential of HTML5, CSS3, and JavaScript technologies with some server-side Languages such as PHP and .NET. Learned web services and APIs namely Youtube, Google Maps, Mailchimp and several others. Expand my knowledge in building workflows and tooling; Grunt, Markdown, SCSS, Angular, EmberJS, etc. Initiated and pioneered knowledge-sharing program.

**MyNappies - Lead Developer (Magento)**
_September 2012 – Decempber 2013_

Worked as an offshore technical partner for an amazingly talented entrepreneur from Australia. Redesigned and extended its e-commerce platform from custom modules and integrations using PHP and Zend, to metrics and social media integrations using various JavaScript APIs, to designing marketing campaign materials. Administered its Linux system; benchmarking website performance, optimization, and troubleshooting.

**Project Assistant - Senior Web Developer/Designer**
_2012_

Manage web projects, Build Wordpress websites, Develop Wordpress Plugins and Integrations (SugarCRM), Magento theming/templating (responsive design), Brand development from designing logos, user interfaces, website layout, brochure and email templates as well as generate company documents.

**Virtual Assistant Technologies - Senior Web Developer**
_2011_

Introduce web development workflow in terms of software, version control, file and server management. Help strategize projects and timelines for web development team. Keeps track of site performance, optimization and general troubleshooting, provide technical support to teams, and generate reports for various functions of the company.

## Select Works

a selection of Jubal's works

- [Dulux](http://dulux.com.au)
- [Head and Shoulders](http://hs-men.jp)
- [SK-II](http://sk-ii.com)
- [Pantene](http://pantene.jp)
- [The Peninsula Hotels](http://peninsula.com)
- [FWD](http://getreadytolive.fwd.com.hk)
- [Eden: Cheeseanything](http://cheeseanything.com)
- [Mogu Mogu](http://mogumogumanila.com/mogumogu)
- [Kelloggs: Crunchy Nut](#)

