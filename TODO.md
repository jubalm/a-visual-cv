## Goals 

- Come up with a unique design that illustrates visual control
- Compress content up in max 2 pages

## Tasks

- [x] Gather design references for resume
- [x] Come up with resume content
- [x] Build in Illustrator
- [x] Save PDF for distribution
- [x] Version Control in bitbucket 
- [x] MS Word Version
- [x] MS Word Plain Text Version
- [x] Fix Typos
